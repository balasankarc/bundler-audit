FROM ruby:2.7-slim

ARG BUNDLER_VERSION="1.17.3"
ENV BUNDLER_VERSION $BUNDLER_VERSION

ARG BUNDLER_AUDIT_VERSION="0.6.1"
ENV BUNDLER_AUDIT_VERSION $BUNDLER_AUDIT_VERSION

ARG BUNDLER_AUDIT_ADVISORY_DB_URL="https://github.com/rubysec/ruby-advisory-db.git"
ARG BUNDLER_AUDIT_ADVISORY_DB_REF_NAME="master"
ENV BUNDLER_AUDIT_ADVISORY_DB_URL $BUNDLER_AUDIT_ADVISORY_DB_URL
ENV BUNDLER_AUDIT_ADVISORY_DB_REF_NAME $BUNDLER_AUDIT_ADVISORY_DB_REF_NAME

RUN set -ex; \
    \
    apt-get update; \
    apt-get install -y --no-install-recommends git; \
    rm -rf /var/lib/apt/lists/*; \
    \
    mkdir -p /root/.local/share/ruby-advisory-db; \
    git clone --branch $BUNDLER_AUDIT_ADVISORY_DB_REF_NAME $BUNDLER_AUDIT_ADVISORY_DB_URL /root/.local/share/ruby-advisory-db; \
    \
    gem install bundler:$BUNDLER_VERSION bundler-audit:$BUNDLER_AUDIT_VERSION; \
    bundle audit update

COPY analyzer /
ENTRYPOINT []
CMD ["/analyzer", "run"]
